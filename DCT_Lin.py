#from glm import column, row
from pyexcel_ods3 import get_data
import sys

PAGE_START_ADDRESS = "0x0801F000"
FLASH_PAGE_SIZE = 32 / 8
FLASH_PAGE_DATA_SIZE = (FLASH_PAGE_SIZE - 1)

#OdsInputFileName = "NVM.ods"
OdsInputFileName = "NVM_Slave.ods"
HexOutputFileName = "output.hex"

helpMessage = "\n**************************************************\n\n"
helpMessage += "\t-o specify .hex output file name\n"
helpMessage += "\t-i specify .ods input file containing NVM data\n"
helpMessage += "\t-a specify Page Start Address format 0x00000000\n"
helpMessage += "\t-s specify FLASH allowed record size [32/64]\n\n"
helpMessage += "**************************************************"
helpFlag = 0

# ****** sys.argv arguments evaluation ********
try:
    for i in range(1, len(sys.argv), 2):
        if sys.argv[i] == "-i":
            OdsInputFileName = sys.argv[i + 1]

        if sys.argv[i] == "-o":
            HexOutputFileName = sys.argv[i + 1]

        if sys.argv[i] == "-a":
            PAGE_START_ADDRESS = sys.argv[i + 1]

        if sys.argv[i] == "-s":
            FLASH_PAGE_SIZE = int(sys.argv[i + 1]) / 8

        if (sys.argv[i] == "-help") or (sys.argv[i] == "-h"):
            helpFlag = 1
            print(helpMessage)
except:
    print("Wrong sys.argv")

if FLASH_PAGE_SIZE != 64 / 8 and FLASH_PAGE_SIZE != 32 / 8:
    print("--- Wrong flash page size ---\nSpecify [64] or [32]")
    quit()
# *********************************************


# ******* End message creation ****************
endMessage = "Intel "
endMessage += HexOutputFileName
endMessage += " file created correctly"
# *********************************************

FLASH_PAGE_DATA_SIZE = (FLASH_PAGE_SIZE - 1)
CRC_COMPLEMENT_NUMBER = 256
END_OF_FILE = ":00000001FF"


# Build enum to paste
def buildNvmEnumList(lNazw):
    file = open("NVM_EntryList.h", "w")

    print("typedef enum {")
    file.write("typedef enum {\n")
    for a in lNazw:
        print("\t", a[1], " = ", a[0])
        file.write("\t" + str(a[1]) + " = " + str(a[0]) + ",\n")
    print("\tNVM_EntryCount")
    file.write("\tNVM_EntryCount\n} NVM_DataId_e;")
    print("} NVM_DataId_e;")


def buildPreDataLine(addressHex="0x00000000"):
    dataBeginVal = 6

    if addressHex[1] == "x":
        firstHalf = addressHex.split("x")[1][0:2]
        secondHalf = addressHex.split("x")[1][2:4]
    else:
        firstHalf = addressHex[0:2]
        secondHalf = addressHex[2:4]

    sumForCRC = (dataBeginVal + int(firstHalf, 16) + int(secondHalf, 16))

    if len(hex(sumForCRC).split("x")[1]) < 2:
        crcString = ("0" + hex(sumForCRC).split("x")[1][-1])
    else:
        crcString = (hex(sumForCRC).split("x")[1][-2] + hex(sumForCRC).split("x")[1][-1])

    crc = CRC_COMPLEMENT_NUMBER - int(crcString, 16)

    returnSt = ":02000004" + firstHalf + secondHalf + hex(crc).split("x")[1]

    return returnSt


def buildDataLine(addressHex="0x00000000", dataString="0000"):
    dataArr = []
    dataSum = 0

    # FLASH memory address
    if addressHex[1] == "x":
        firstHalf = addressHex.split("x")[1][4:6]
        secondHalf = addressHex.split("x")[1][6:8]
    else:
        firstHalf = addressHex[4:6]
        secondHalf = addressHex[6:8]

    # adding data elements
    for dataElement in range(0, len(dataString), 2):
        dataArr.append(dataString[dataElement] + dataString[dataElement + 1])

    for dataElement in dataArr:
        dataSum += int(dataElement, 16)

    sumForCRC = (len(dataArr) + int(firstHalf, 16) + int(secondHalf, 16) + dataSum)

    if len(hex(sumForCRC).split("x")[1]) < 2:
        crcString = ("0" + hex(sumForCRC).split("x")[1][-1])
    else:
        crcString = (hex(sumForCRC).split("x")[1][-2] + hex(sumForCRC).split("x")[1][-1])

    crc = CRC_COMPLEMENT_NUMBER - int(crcString, 16)

    if len(str(crc)) == 1:
        s_crc = "0" + str(crc)
    else:
        s_crc = hex(crc).split("x")[1]

    returnSt = ":" + normalizeHexValue((hex(len(dataArr)).split("x")[1]),
                                       2) + firstHalf + secondHalf + "00" + dataString + s_crc

    return returnSt


def buildHexFile(address="0x00000000", dataArray=None):
    if dataArray is None:
        dataArray = []
    hexFile = open(HexOutputFileName, "w")

    lineAddr = int(address, 16)

    preDataLine = buildPreDataLine(normalizeHexValue(hex(lineAddr), 8))

    hexFile.write(preDataLine + "\n")

    for entry in dataArray:
        if len(entry) > 0:
            dataLine = buildDataLine(normalizeHexValue(hex(lineAddr), 8), entry)
            hexFile.write(dataLine + "\n")
        lineAddr += 16

    hexFile.write(END_OF_FILE)

    hexFile.close()


def applyByteEndian(stringNumber):
    retStr = ""

    for element in range((len(stringNumber) - 2), -1, -2):
        retStr += stringNumber[element] + stringNumber[element + 1]

    return retStr


def normalizeHexValue(hexString, length):
    retStr = ""

    if (len(hexString) > 2) and (hexString[1] == "x"):
        hexNumber = hexString
    else:
        hexNumber = "0x" + hexString

    if len(hexNumber.split("x")[1]) < length:
        actLen = len(hexNumber.split("x")[1])
        retStr = ("0" * (length - actLen)) + hexNumber.split("x")[1]
    else:
        retStr = hexNumber.split("x")[1]

    if length == 4:
        return applyByteEndian(retStr)
    else:
        return retStr


def buildDataLines(valuesList):
    dataLines = []
    counter = 0
    columnCounter = 0
    rowCounter = 0
    dataLines.append("")
    valStr = ""

    for zeros in range(8):
        dataLines[0] += normalizeHexValue(hex(0), 2)
        columnCounter += 1

    for val in range(len(valuesList)):

        dataLines[rowCounter] += normalizeHexValue(hex(val), 2)
        columnCounter += 1

        for col in range(int(FLASH_PAGE_DATA_SIZE)):
            valStr = normalizeHexValue(hex(valuesList[val][col]), 2)
            dataLines[rowCounter] += valStr

            columnCounter += 1

            if columnCounter == 16:
                columnCounter = 0
                rowCounter += 1
                dataLines.append("")

    return dataLines


def getItemsFromFile(fileName="NVM.ods"):
    itemsArr = []

    # read data from file
    data = get_data(fileName)

    # find all rows od NVM entities and pack them into an array
    for a, b in data.items():
        for item in b[1:]:
            itemsArr.append(item)

    return itemsArr


def getFinalData(dataLinesArr):
    nameList = []
    valuesList = []

    localCounter = 0
    for lines in dataLinesArr:
        if lines:
            # data for enum build
            nameList.append([localCounter, lines[0]])

            # values list
            valuesList.append([localCounter])

            # positioning in table
            # counters count from 0 and excel indexes from 1
            for cnt in range(int(FLASH_PAGE_DATA_SIZE) - 1):
                valuesList[localCounter].extend([lines[cnt + 2]])

            localCounter += 1
    return nameList, valuesList


if __name__ == "__main__":

    if not helpFlag:
        nvmEntryNameList = []
        nvmEntryValuesList = []
        fileData = []

        nvmEntryNameList, nvmEntryValuesList = getFinalData(getItemsFromFile(OdsInputFileName))

        buildNvmEnumList(nvmEntryNameList)

        buildHexFile(PAGE_START_ADDRESS, buildDataLines(nvmEntryValuesList))

    print(endMessage)
